History
=======

1.4 (2023-11-29)
----------------
* C-Version rhs: replaced `Rf_error(buffer);` with `Rf_error("%s", buffer);`
* in R/streambugs_aux.r: changed name of the function exp.transform to exp_transform to avoid confusion with S3 methods

1.3 (2023-01-23)
----------------
* C-Version rhs: replaced deprecated `sprintf` with safer `snprintf`

1.2 (2020-04-09)
------------------
* Re-factoring and vectorization of functions parsing input and parameter values, plus
  regression and unit tests for these.
* Minor bugfixes.

1.1 (2019-07-12)
------------------

* Added extended example model.
* Added food web plot function.
* Added feeding links count.
* S3 `plot` dispatching works now on `run.streambugs` results.

1.0 (2017-11-07)
------------------

* The model version 1.0 <DOI:10.1890/12-0591.1> with extensions and the core functions
  <DOI:10.1021/acs.est.5b04068>, <DOI:10.1002/eap.1530>, <DOI:10.1111/fwb.12927>.
